const fs = require("fs");

let storage = function() {
    const fileName = __dirname + "/../data.json";

    var isFileExist = function() {
        try {
            return fs.existsSync(fileName);
        } catch (err) {
            console.log(err);
            return false;
        }
    }



    findIndexFromId = function(items, id) {
        var resp = items.map(function(item, index) {
            if (item.id == id) {
                return index;
            }
        }).filter(function(item) {
            return item !== undefined;
        });

        return resp.length > 0 ? resp[0] : null;
    }

    this.findAll = function() {
        var response = [];
        if (isFileExist()) {
            var fileContent = fs.readFileSync(fileName);
            if (fileContent.length) {
                response = JSON.parse(fileContent);
            }
        }
        return response;
    }

    this.findOne = function(id) {
        var resp = this.findAll().filter(function(item) {
            return item.id == id;
        });
        return resp.length > 0 ? resp[0] : null;
    }

    this.add = function(name) {
        var newID = 1,
            items = this.findAll();
        if (items.length) {
            newID = items[items.length - 1].id + 1;
        }
        items.push({
            id: newID,
            name: name
        });
        fs.writeFileSync(fileName, JSON.stringify(items));
    }
    this.remove = function(id) {
        var items = this.findAll(),
            index = findIndexFromId(items, id);
        if (index !== null) {
            items.splice(index, 1);
        }
        fs.writeFileSync(fileName, JSON.stringify(items));
    }
}

module.exports = storage;