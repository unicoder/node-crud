const mongoose = require("mongoose")
mongoose.connect('mongodb://localhost/crud_db', function(err) {
    if (err) throw err;
});

const newsSchema = mongoose.Schema({
    _id: mongoose.Schema.Types.ObjectId,
    name: {
        type: String,
        required: true
    },
    created: {
        type: Date,
        default: Date.now
    }
});

let storage = function() {

    const NewsModel = mongoose.model('News', newsSchema);

    this.findAll = function(callback) {
        NewsModel.find().exec(function(err, items) {
            var response = items.map(function(item) {
                return {
                    id: item._id,
                    name: item.name
                };
            })
            if (typeof callback == "function") {
                callback(response);
            }
        });
    }

    this.findOne = function(id, callback) {
        NewsModel.findById(id, function(err, item) {
            if (!err) {
                var response = {
                    id: item._id,
                    name: item.name
                }
                if (typeof callback == "function") {
                    callback(response);
                }
            }
        })
    }

    this.add = function(name, callback) {
        var model = new NewsModel({
            _id: new mongoose.Types.ObjectId(),
            name: name
        });
        model.save(function(err) {
            if (err) throw err;
            if (typeof callback == "function") {
                callback()
            }
        });
    }
    this.edit = function(id, name, callback) {
        NewsModel.findByIdAndUpdate(id, {
            name: name
        }, function(err) {
            if (typeof callback == "function") {
                callback()
            }
        })
    }
    this.remove = function(id, callback) {
        NewsModel.findByIdAndDelete(id, function(err) {
            if (!err) {
                if (typeof callback == "function") {
                    callback();
                }
            }
        })
    }
}

module.exports = storage;