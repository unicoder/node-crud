var express = require("express"),
    app = express(),
    bodyParser = require("body-parser"),
    sm = require("./storage/_mongoose"),
    storage = new sm();

const urlencodedParser = bodyParser.urlencoded({ extended: false });

app.engine("ejs", require("ejs-locals"));
app.set("views", __dirname + "/templates");
app.set("view engine", "ejs");

//create
app.get("/create.html", urlencodedParser, function(request, response) {
    response.render("views/news/create");
});
app.post("/create.html", urlencodedParser, function(request, response) {
    if (!request.body) return response.sendStatus(400);
    var form = request.body;
    storage.add(form.name_of_news, function() {
        response.redirect("/");
    });

});
//update
app.get("/update/:id.html", function(request, response) {
    storage.findOne(request.params.id, function(item) {
        response.render("views/news/update", {
            "item": item
        });
    })
});
app.post("/update/:id.html", urlencodedParser, function(request, response) {
    if (!request.body) return response.sendStatus(400);
    var form = request.body;
    storage.edit(
        request.params.id,
        form.name_of_news,
        function() {
            response.redirect("/");
        }
    )
});
//delete
app.get("/delete/:id.html", function(request, response) {
    storage.remove(request.params.id, function() {
        response.redirect("/");
    });

});
//read-all
app.get("/", function(request, response) {
    storage.findAll(function(items) {
        response.render("views/news/index", {
            "items": items
        });
    });
});
//read-one
app.get("/:id.html", function(request, response) {
    storage.findOne(request.params.id, function(item) {
        response.send(item);
        // response.render("views/news/view", {
        //     "item": item
        // });
    })
});

app.listen(8777, function() {
    console.log('Я стартанул');
});


// findAll
// findOne(id)
// add(data)
// edit(id, data)
// remove(id)